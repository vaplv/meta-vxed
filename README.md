# Meta VxEd project

This code base provides a [CMake](http://www.cmake.org) script to help in the
setup of the [VxEd](https://gitorious.org/code-plot/vxed/) developpement
environment. It checkouts, configures, builds and installs the VxEd
dependencies before cloning and configuring it.

## Quick start

Generate the CMake project defined in the `meta` directory and build it. Go to
the `vxed/build` directory to build the VxEd project or into `vxed/src` to edit
its sources.

